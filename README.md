

```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
import stumpy
import pickle
from MatrixProfilePatternDetector import MatrixProfilePatternDetector as mppd
from scipy import signal
from dask.distributed import Client
dask_client = Client()
```


# Example


```python
def butt_filter(T, order, freq, btype = "low"): #Butterworth filter
    """ 
    T - timeseries,
    order - the order of the filter.
    freq - cut-off frequency of the filter
    btype - the type of filter
    """
    fs = len(T)
    fc = freq  
    w = fc / (fs / 2)
    b, a = signal.butter(order, w, btype)
    output = signal.filtfilt(b, a, T)
    return output
```


```python
data = pd.read_csv("tep_data.csv", header = None)
```


```python
t1 = data[1].values
plt.figure(figsize=[16,6])
plt.title("With out filter")
plt.plot(t1)
plt.figure(figsize=[16,6])
plt.title("With out filter ZOOMED")
plt.plot(t1[:100])
```




    [<matplotlib.lines.Line2D at 0x7f665b239128>]




![png](images/output_4_1.png)



![png](images/output_4_2.png)



```python
ft1 = t1 - butt_filter(t1, order = 5, freq = 3500, btype = 'low')
plt.figure(figsize=[16,6])
plt.title("With filter")
plt.plot(ft1)
plt.figure(figsize=[16,6])
plt.title("With filter ZOOMED")
plt.plot(ft1[:100])
```




    [<matplotlib.lines.Line2D at 0x7f665b121908>]




![png](images/output_5_1.png)



![png](images/output_5_2.png)



```python
print("Length of timeseries -", len(ft1))
```

    Length of timeseries - 12801



```python
start_time = time.time()
a = mppd(timeseries = ft1)
a.calculate_matrix_profile(window_size = 10)
print("Time: ", time.time() - start_time)
```

    Min value of euclidian distance -  0.11818513579238181
    Median value of euclidian distance -  0.5865196212451155
    Max value of euclidian distance -  1.7005604316497613
    Time:  0.5109553337097168



```python
start_time = time.time()
a.get_patterns(threshold = 0.5, reduction_ratio = 0.96, mode = "normal")
print("Time: ", time.time() - start_time)
```

    Time:  0.06831693649291992



```python
print(len(a.patterns))
```

    536



```python
print(len(a.patterns[0]))
```

    40



```python
a.plot_patterns(a.patterns[:5] ,max_quantity = 10)
```


![png](images/output_11_0.png)



```python
a.mark_up_patterns([a.patterns[0]], pass_indexes = 1)
```


![png](images/output_12_0.png)



```python
a.mark_up_patterns([a.patterns[0]], zoom = [2000 - 50,2000 + 200])
```


![png](images/output_13_0.png)



```python
a.mp[11467]
```




    array([0.7564692329275357, 0, 0, 12641], dtype=object)




```python

```
